<!DOCTYPE html>
<html lang="en">
    <head>
        @include('templates.front-end.meta-loader')
        @include('templates.front-end.css-loader')
    </head>
    <body>
        <!-- Page Preloder -->
        <div id="preloder">
            <div class="loader"></div>
        </div>
        @include('templates.front-end.header-mobile')
        @include('templates.front-end.header')
        <div class="container">
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Debitis autem fuga aperiam repellendus? Pariatur, corporis. Praesentium, quidem aspernatur? Facilis libero delectus aperiam totam corrupti a ipsum perferendis rerum, dicta eos.
        </div>

        @include('templates.front-end.footer')
        @include('templates.front-end.js-loader')
    </body>
</html>
