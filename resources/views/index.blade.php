<!DOCTYPE html>
<html lang="id">
    <head>
        @include('templates.front-end.meta-loader')
        @include('templates.front-end.css-loader')
    </head>

    <body>
        <!-- Page Preloder -->
        <div id="preloder">
            <div class="loader"></div>
        </div>

        @include('templates.front-end.header-mobile')
        @include('templates.front-end.header')

        <!-- Hero Section Begin -->
        <section
            class="hero-section set-bg"
            data-setbg="assets/img/company/homeBanner.jpg"
        >
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="hero-text">
                            <h2>Mobil mogok karena Aki?</h2>
                            <h1>Hubungi HALIM ACCU!</h1>
                            <h4 style="color: #ffcc00; font-size: 18px">
                                Buka 24 jam dan siap antar.
                            </h4>
                            <div style="clear: both; margin-bottom: 35px"></div>
                            <a href="#" class="primary-btn"
                                >Pesan Aki Sekarang</a
                            >
                        </div>
                    </div>
                </div>
            </div>
            <!-- hand hold phone -->
            <div class="">
                <div class="">
                    <div class="overlay"></div>
                    <img
                        class="hand-hold-phone"
                        src="assets/img/hand-hold-phone.png"
                        alt=""
                    />
                </div>
            </div>
        </section>
        <!-- Hero Section End -->

        <!-- About Us Section Begin -->
        <section class="aboutus-section spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="about-text">
                            <div class="section-title">
                                <span>About Us</span>
                                <h2>Halim Accu</h2>
                            </div>
                            <p
                                class="f-para tj indent"
                                style="line-height: 35px"
                            >
                                Halim Accu adalah toko baterai yang bergerak di
                                bidang kendaraan seperti mobil, motor, UPS,
                                genset, dll. Kami berusaha memberikan solusi
                                atas semua kebutuhan aki Anda. Dengan slogan
                                “Delivery and ready to use” kami berkomitmen
                                memberikan pelayanan terbaik serta yang maksimal
                                untuk Anda.
                            </p>
                            <p
                                class="s-para tj indent"
                                style="line-height: 35px"
                            >
                                Kami menyediakan layanan pengiriman 24 jam dan
                                siap antar untuk menyelesaikan masalah aki Anda.
                                Melayani setiap hari termasuk tanggal merah.
                                Free konsultasi melalui telpon/whatsapp.
                            </p>
                            <div class="tc">
                                <a
                                    href="{{ route('guest.about') }}"
                                    class="primary-btn about-btn"
                                    >Read More</a
                                >
                            </div>
                        </div>
                    </div>
                    <div class="col-10 center col-lg-6">
                        <video
                            id="my-player"
                            class="video-js"
                            controls
                            preload="auto"
                            poster="assets/video/company-profile-thumb.png"
                        >
                            <source
                                src="assets/video/company-profile.mp4"
                                type="video/mp4"
                            />
                            <p class="vjs-no-js">
                                To view this video please enable JavaScript, and
                                consider upgrading to a web browser that
                                <a
                                    href="https://videojs.com/html5-video-support/"
                                    target="_blank"
                                >
                                    supports HTML5 video
                                </a>
                            </p>
                        </video>
                    </div>
                </div>
            </div>
        </section>
        <!-- About Us Section End -->

        <!-- Services Section End -->
        <section class="services-section spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title">
                            <h2 class="relative">
                                <div class="white">Layanan Kami</div>
                                <div class="underline center"></div>
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div
                        class="col-lg-3 col-sm-5"
                        data-aos="fade-in"
                        data-aos-once="true"
                        data-aos-delay="200"
                    >
                        <div class="service-item">
                            <img
                                src="assets/img/icon/timer-flash-line.svg"
                                class="icon"
                                alt=""
                            />
                            <h4>Buka Setiap Hari</h4>
                            <p class="b mb1 title">
                                Setiap hari pukul : 07.00 - 22.00 WIB.
                            </p>
                            <p class="moon-gray mt2">
                                Khusus hari besar : Libur / Ada pemberitahuan.
                            </p>
                        </div>
                    </div>
                    <div
                        class="col-lg-3 col-sm-5"
                        data-aos="fade-in"
                        data-aos-once="true"
                        data-aos-delay="300"
                    >
                        <div class="service-item">
                            <img
                                src="assets/img/icon/truck-line.svg"
                                class="icon"
                                alt=""
                            />
                            <h4>Layanan 24 Jam</h4>
                            <p class="mb1 b title">
                                Kami memberikan layanan 24 jam.
                            </p>
                            <p class="moon-gray mt2">
                                Hubungi <br />
                                085-731-545-946 (Rifqi) / <br />
                                085-746-546-073 (Halim)
                            </p>
                        </div>
                    </div>
                    <div
                        class="col-lg-3 col-sm-5"
                        data-aos="fade-in"
                        data-aos-once="true"
                        data-aos-delay="400"
                    >
                        <div class="service-item">
                            <img
                                src="assets/img/icon/customer-service.svg"
                                class="icon"
                                alt=""
                            />
                            <h4>Layanan Unggul</h4>
                            <p class="mb1 b title">
                                Gratis layanan antar dan pasang Aki di hari
                                kerja dan jam kerja.
                            </p>
                            <p class="moon-gray mt2">
                                Diluar itu dikenakan biaya kirim.
                            </p>
                        </div>
                    </div>
                    <div
                        class="col-lg-3 col-sm-5"
                        data-aos="fade-in"
                        data-aos-once="true"
                        data-aos-delay="500"
                    >
                        <div class="service-item">
                            <img
                                src="assets/img/icon/medal-line.svg"
                                class="icon"
                                alt=""
                            />
                            <h4>After Sales</h4>
                            <p class="mb1 b title">
                                Gratis pengecekan aki & voltase pengecasan
                            </p>
                            <p class="moon-gray mt2">
                                Setiap pembelian aki, konsumen berhak
                                mendapatkan layanan pengecekan aki & pemeriksaan
                                voltase pengecasan.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Services Section End -->

        <!-- Produk Kami -->
        <section class="hp-room-section spad">
            <div class="container">
                <div class="hp-room-items">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-title">
                                <h2 class="relative">
                                    <div class="black">Produk Kami</div>
                                    <div class="underline center"></div>
                                </h2>
                            </div>
                        </div>
                    </div>
                    <!-- home product slider -->
                    <div class="swiper-container" id="home-product-slider">
                        <!-- Additional required wrapper -->
                        <div class="swiper-wrapper mb3">
                            <!-- Slides -->
                            <div class="swiper-slide">
                                <div class="col">
                                    <div
                                        class="hp-room-item set-bg"
                                        data-setbg="assets/img/products/gsAstra.png"
                                    >
                                        <div class="hr-text flex flex-column">
                                            <!-- <h3>NS40Z & NS40ZL (36B24R/L)</h3> -->
                                            <table class="produk-text">
                                                <h4>AKI GS ASTRA</h4>
                                                <tbody>
                                                    <tr>
                                                        <td class="r-o">
                                                            Kapasitas :
                                                        </td>
                                                        <td>35 Ah</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="r-o">
                                                            Tegangan :
                                                        </td>
                                                        <td>12 V (Volt)</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="r-o">
                                                            Kategori :
                                                        </td>
                                                        <td>
                                                            Aki kering/MF
                                                            (Maintenance Free)
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <a href="#" class="primary-btn"
                                                >More Details</a
                                            >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="col">
                                    <div
                                        class="hp-room-item set-bg"
                                        data-setbg="assets/img/products/gsPremium.png"
                                    >
                                        <div class="hr-text flex flex-column">
                                            <!-- <h3>NS40Z & NS40ZL (36B24R/L)</h3> -->
                                            <h4>AKI GS ASTRA</h4>
                                            <table class="produk-text">
                                                <tbody>
                                                    <tr>
                                                        <td class="r-o">
                                                            Kapasitas :
                                                        </td>
                                                        <td>35 Ah</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="r-o">
                                                            Tegangan :
                                                        </td>
                                                        <td>12V (Volt)</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="r-o">
                                                            Kategori :
                                                        </td>
                                                        <td>
                                                            Aki
                                                            basah/Hybrid/Premium
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <a href="#" class="primary-btn"
                                                >More Details</a
                                            >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="col">
                                    <div
                                        class="hp-room-item set-bg"
                                        data-setbg="assets/img/products/gsHybrid.png"
                                    >
                                        <div class="hr-text flex flex-column">
                                            <h4>AKI GS ASTRA</h4>
                                            <table class="produk-text">
                                                <tbody>
                                                    <tr>
                                                        <td class="r-o">
                                                            Kapasitas :
                                                        </td>
                                                        <td>45 Ah</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="r-o">
                                                            Tegangan :
                                                        </td>
                                                        <td>12V (Volt)</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="r-o">
                                                            Kategori :
                                                        </td>
                                                        <td>
                                                            Aki
                                                            basah/Hybrid/Premium
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <a href="#" class="primary-btn"
                                                >More Details</a
                                            >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="col">
                                    <div
                                        class="hp-room-item set-bg"
                                        data-setbg="assets/img/products/gsMf.png"
                                    >
                                        <div class="hr-text flex flex-column">
                                            <!-- <h3>NS60 & NS60L (46B24R/L)</h3> -->
                                            <h4>AKI GS ASTRA</h4>
                                            <table class="produk-text">
                                                <tbody>
                                                    <tr>
                                                        <td class="r-o">
                                                            Kapasitas :
                                                        </td>
                                                        <td>45 Ah</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="r-o">
                                                            Tegangan :
                                                        </td>
                                                        <td>12V (volt)</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="r-o">
                                                            Kategori :
                                                        </td>
                                                        <td>
                                                            Aki kering/MF
                                                            (Maintenance Free)
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <a href="#" class="primary-btn"
                                                >More Details</a
                                            >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="col">
                                    <div
                                        class="hp-room-item set-bg"
                                        data-setbg="assets/img/products/gsMf.png"
                                    >
                                        <div class="hr-text flex flex-column">
                                            <!-- <h3>NS60 & NS60L (46B24R/L)</h3> -->
                                            <h4>AKI GS ASTRA</h4>
                                            <table class="produk-text">
                                                <tbody>
                                                    <tr>
                                                        <td class="r-o">
                                                            Kapasitas :
                                                        </td>
                                                        <td>45 Ah</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="r-o">
                                                            Tegangan :
                                                        </td>
                                                        <td>12V (volt)</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="r-o">
                                                            Kategori :
                                                        </td>
                                                        <td>
                                                            Aki kering/MF
                                                            (Maintenance Free)
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <a href="#" class="primary-btn"
                                                >More Details</a
                                            >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="col">
                                    <div
                                        class="hp-room-item set-bg"
                                        data-setbg="assets/img/products/gsMf.png"
                                    >
                                        <div class="hr-text flex flex-column">
                                            <!-- <h3>NS60 & NS60L (46B24R/L)</h3> -->
                                            <h4>AKI GS ASTRA</h4>
                                            <table class="produk-text">
                                                <tbody>
                                                    <tr>
                                                        <td class="r-o">
                                                            Kapasitas :
                                                        </td>
                                                        <td>45 Ah</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="r-o">
                                                            Tegangan :
                                                        </td>
                                                        <td>12V (volt)</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="r-o">
                                                            Kategori :
                                                        </td>
                                                        <td>
                                                            Aki kering/MF
                                                            (Maintenance Free)
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <a href="#" class="primary-btn"
                                                >More Details</a
                                            >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- If we need pagination -->
                        <div class="swiper-pagination"></div>
                        <!-- If we need navigation buttons -->
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Home Room Section End -->

        <!-- number counter -->

        <section
            class="spad counter-area"
            data-aos="fade-up"
            data-aos-id="counter-area"
            data-aos-once="true"
        >
            <div class="container">
                <div class="pb4">
                    <div class="section-title">
                        <h2 class="relative">
                            <div class="black">Statistik</div>
                            <div class="underline center"></div>
                        </h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <div class="card card-counter">
                            <div class="card-body">
                                <h5 class="card-title" id="counter-1"></h5>
                                <p class="card-text">Jumlah Accu Terjual</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="card card-counter">
                            <div class="card-body">
                                <h5 class="card-title" id="counter-2"></h5>
                                <p class="card-text">Jumlah Accu Terjual</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="card card-counter">
                            <div class="card-body">
                                <h5 class="card-title" id="counter-3"></h5>
                                <p class="card-text">Jumlah Accu Terjual</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="card card-counter">
                            <div class="card-body">
                                <h5 class="card-title" id="counter-4"></h5>
                                <p class="card-text">Jumlah Accu Terjual</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Testimonial Section Begin -->
        <section class="testimonial-section spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title">
                            <h2>Gallery</h2>
                            <div class="underline center"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <div
                            class="testimonial-slider custom-slider owl-carousel"
                        >
                            <div class="ts-item">
                                <img
                                    src="assets/img/gallery/halim-accu-img-1.jpg"
                                    alt=""
                                />
                            </div>
                            <div class="ts-item">
                                <img
                                    src="assets/img/gallery/halim-accu-img-2.jpg"
                                    alt=""
                                />
                            </div>
                            <div class="ts-item">
                                <img
                                    src="assets/img/gallery/halim-accu-img-3.jpg"
                                    alt=""
                                />
                            </div>
                            <div class="ts-item">
                                <img
                                    src="assets/img/gallery/halim-accu-img-4.jpg"
                                    alt=""
                                />
                            </div>
                            <div class="ts-item">
                                <img
                                    src="assets/img/gallery/halim-accu-img-5.jpg"
                                    alt=""
                                />
                            </div>
                            <div class="ts-item">
                                <img
                                    src="assets/img/gallery/halim-accu-img-6.jpg"
                                    alt=""
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Testimonial Section End -->

        <!-- Blog Section Begin -->
        <section class="blog-section spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title">
                            <h2 class="relative">
                                <div class="black">Artikel</div>
                                <div class="underline center"></div>
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div
                            class="blog-item flex"
                            style="background-color: #117688"
                        >
                            <div class="mt6">
                                <img src="assets/img/company/logo.png" alt="" />
                            </div>
                            <div class="bi-text">
                                <span class="b-tag">Pengetahuan</span>
                                <h4>
                                    <a
                                        href="{{
                                            route('guest.accu-definition')
                                        }}"
                                        >Pengertian Accu</a
                                    >
                                </h4>
                                <div class="b-time">
                                    <i class="icon_clock_alt"></i> 15th March,
                                    2020
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div
                            class="blog-item flex"
                            style="background-color: #117688"
                        >
                            <div class="mt6">
                                <img src="assets/img/company/logo.png" alt="" />
                            </div>
                            <div class="bi-text">
                                <span class="b-tag">Pengetahuan</span>
                                <h4>
                                    <a
                                        href="{{
                                            route('guest.accu-series-code')
                                        }}"
                                        >Mengenal Kode Seri Pada Aki</a
                                    >
                                </h4>
                                <div class="b-time">
                                    <i class="icon_clock_alt"></i> 15th March,
                                    2020
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Blog Section End -->

        @include('templates.front-end.footer')

        <!-- Search model Begin -->
        <div class="search-model">
            <div class="h-100 d-flex align-items-center justify-content-center">
                <div class="search-close-switch">
                    <i class="icon_close"></i>
                </div>
                <form class="search-model-form">
                    <input
                        type="text"
                        id="search-input"
                        placeholder="Search here....."
                    />
                </form>
            </div>
        </div>
        <!-- Search model end -->

        @include('templates.front-end.js-loader')
    </body>
</html>
