<!DOCTYPE html>
<html lang="en">
<head>
    @include('templates.front-end.meta-loader')

    @include('templates.front-end.css-loader')
</head>
<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>
    @include('templates.front-end.header-mobile')

    @include('templates.front-end.header')

    
    <!-- <div id="map"></div> -->
    <section class="mapsSection">
        <div class="container-fluid">
            <div class="row">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.0686137856055!2d112.1809394142137!3d-8.094485683191152!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e78ede609572567%3A0xcc5b9271276a03f8!2sHALIM%20ACCU%20MOBIL%26MOTOR%3B%20SIAP%20ANTAR%3BFREE%20ONGKOS%20PASANG%20%26CEK%20AKI!5e0!3m2!1sid!2sid!4v1584806912348!5m2!1sid!2sid" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
        </div>
    </section>

    <section class="addressSection">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="assets/img/company/img01.png" alt="">
                </div>
                <div class="col-md-6">
                    <div class="section-title">
                        <h3>Alamat kami</h3>
                    </div>
                    <ul style="list-style: none;">
                        <li>
                            <p class="f-para" style="text-align: center">Jl. Imam Bonjol No.69, Sananwetan, Kec. Sananwetan, Kota Blitar, Jawa Timur 66137</p>
                        </li>
                        <li>
                            <p class="f-para" style="text-align: center">Jl. Imam Bonjol No.69, Sananwetan, Kec. Sananwetan, Kota Blitar, Jawa Timur 66137</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    @include('templates.front-end.footer')

    @include('templates.front-end.js-loader')
</body>
</html>