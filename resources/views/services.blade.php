<!DOCTYPE html>
<html lang="en">
    <head>
        @include('templates.front-end.meta-loader')
        @include('templates.front-end.css-loader')
    </head>
    <body>
        <!-- Page Preloder -->
        <div id="preloder">
            <div class="loader"></div>
        </div>
        @include('templates.front-end.header-mobile')
        @include('templates.front-end.header')

        <div
            class="relative flex"
            style="
                background-image: url(assets/img/page-banner.jpg);
                height: 220px;
            "
        >
            <div
                class="absolute w-100 h-100 flex"
                style="height: 200px; background-color: rgba(0, 0, 0, 0.3);"
            >
                <div class="container flex">
                    <div
                        class="flex flex-column items-center justify-center w-100"
                    >
                        <h2 class="white">Services</h2>
                        <p class="f4 mt3">
                            <a class="text-white" href="">Halim Accu</a>
                            <span>/</span>
                            <a class="text-white" href="">Services</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <h3 class="mv5 tc">Our Services</h3>
        <section class="">
            <div class="container">
                <!-- Services grid -->
                <div class="row mv4">
                    <div class="col-sm-12">
                        <div class="row">
                            <div
                                class="col-lg-4 col-sm-6 bb br b--moon-gray pt3"
                            >
                                <div class="tc">
                                    <img
                                        src="assets/img/icon/timer-flash-line.svg"
                                        style="width: 40px;"
                                        alt=""
                                    />
                                    <p class="b f4">Buka Setiap Hari</p>
                                    <p>
                                        Setiap hari pukul : 07.00 - 22.00 WIB.
                                        Khusus hari besar : Libur / Ada
                                        pemberitahuan.
                                    </p>
                                </div>
                            </div>
                            <div
                                class="col-lg-4 col-sm-6 bb br b--moon-gray pt3"
                            >
                                <div class="tc">
                                    <img
                                        src="assets/img/icon/truck-line.svg"
                                        style="width: 40px;"
                                        alt=""
                                    />
                                    <p class="b f4">Layanan Antar 24 Jam</p>
                                    <p>
                                        Kami memberikan layanan 24 jam. Call
                                        center : : 085-731-545-946 atau melalui
                                        mobile phone di nomor 085-746-546-073.
                                    </p>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-6 bb b--moon-gray pt3">
                                <div class="tc">
                                    <img
                                        src="assets/img/icon/customer-service.svg"
                                        style="width: 40px;"
                                        alt=""
                                    />
                                    <p class="b f4">Layanan Unggul</p>
                                    <p>
                                        Gratis layanan antar dan pasang Aki di
                                        hari kerja dan jam kerja. Diluar itu
                                        dikenakan biaya kirim.
                                    </p>
                                </div>
                            </div>
                            <div
                                class="col-lg-4 col-sm-6 bb br b--moon-gray pt3"
                            >
                                <div class="tc">
                                    <img
                                        src="assets/img/icon/medal-line.svg"
                                        style="width: 40px;"
                                        alt=""
                                    />
                                    <p class="b f4">Pembayaran Mudah</p>
                                    <p>
                                        Pembayaran yang sangat mudah bisa
                                        menggunakan debit BCA,BRI,Mandiri
                                    </p>
                                </div>
                            </div>
                            <div
                                class="col-lg-4 col-sm-6 bb br b--moon-gray pt3"
                            >
                                <div class="tc">
                                    <img
                                        src="assets/img/icon/medal-line.svg"
                                        style="width: 40px;"
                                        alt=""
                                    />
                                    <p class="b f4">Gratis Konsultasi</p>
                                    <ol class="tl w-50 center">
                                        <li>Gratis Pengecekan</li>
                                        <li>Free Delivery Area Malang</li>
                                        <li>Menerima Panggilan</li>
                                    </ol>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-6 bb b--moon-gray pt3">
                                <div class="tc">
                                    <img
                                        src="assets/img/icon/medal-line.svg"
                                        style="width: 40px;"
                                        alt=""
                                    />
                                    <p class="b f4">After Sales</p>
                                    <p>
                                        Setiap pembelian aki, konsumen berhak
                                        mendapatkan layanan after sales berupa
                                        pengecekan aki, pemeriksaan voltase
                                        pengecasan.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- service text -->
                <div class="row mt4 mb6">
                    <di class="col-md-12 paraph">
                        <p class="indent">
                            Untuk sebuah Kendaraan kususnya Mobil Kelistrikan
                            adalah hal yang sangat penting. Dalam dunia otomotif
                            mobil, motor dan semua yang menggunakan kelistrikan
                            untuk menggerakkan komponen-komponen sehingga dapat
                            berjalan sesuai dengan kinerja mesin. Adanya suplay
                            listrik yang stabil akan menjaga kestabilan dan
                            kemampuan kerja mesin, aki (accu) sebagai penyimpan
                            energi listrik yang merupakan sumber listrik untuk
                            supply ke starter, sistem pengapian, lampu-lampu dan
                            komponen-komponen lain yang membutuhkan tenaga
                            listrik. Aki (accu) macam-macam kapasitas
                            penyimpanan tenaga listriknya itu tergantung pada
                            kapasitas baterai dalam satuan amper jam (AH). Jika
                            pada kotak baterai tertulis 12 volt 65 AH, berarti
                            baterai tersebut memiliki tegangan 12 volt dimana
                            jika baterai tersebut digunakan selama 1 jam dengan
                            arus pemakaian 65 amper.
                        </p>
                        <div class="mt3">

                            <p class="indent">
                                Kira-kira 2 sampai 2,1 volt. Tegangan listrik yang
                                dihasilkan sama dengan jumlah tegangan listrik
                                tiap-tiap selnya. Jika baterai memiliki 6 sel maka
                                tegangan yang akan dihasilkan 12 volt sampai 12,6
                                volt, umum setiap sel dengan adanya lubang di kotak
                                accu bagian atas untuk mengisi elektrolit aki.
                            </p>
                        </div>
                    </di>
                </div>
            </div>
        </section>

        @include('templates.front-end.footer')
        @include('templates.front-end.js-loader')
    </body>
</html>
