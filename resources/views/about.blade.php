<!DOCTYPE html>
<html lang="en">
    <head>
        @include('templates.front-end.meta-loader')
        @include('templates.front-end.css-loader')
    </head>
    <body>
        <!-- Page Preloder -->
        <div id="preloder">
            <div class="loader"></div>
        </div>
        @include('templates.front-end.header-mobile')
        @include('templates.front-end.header')

        <div
            class="relative flex"
            style="
                background-image: url(assets/img/page-banner.jpg);
                height: 220px;
            "
        >
            <div
                class="absolute w-100 h-100 flex"
                style="height: 200px; background-color: rgba(0, 0, 0, 0.3);"
            >
                <div class="container flex">
                    <div
                        class="flex flex-column items-center justify-center w-100"
                    >
                        <h2 class="white">About Us</h2>
                        <p class="f4 mt3">
                            <a class="text-white" href="">Halim Accu</a>
                            <span>/</span>
                            <a class="text-white" href="">About Us</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <section class="aboutSection pt5">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <h3 class="mb3 tc tl-ns">Visi</h3>
                            <ul style="padding-left: 20px;">
                                <li>
                                    Membantu dan memberikan solusi semua
                                    kebutuhan Aki
                                </li>
                                <li>
                                    Unggul dalam pelayanan dan profesional dalam
                                    pengelolaan
                                </li>
                            </ul>
                        </div>
                        <br />
                        <div class="col-md-12">
                            <h3 class="mb3 tc tl-ns">Misi</h3>
                            <ul style="padding-left: 20px;">
                                <li>
                                    Mempermudah dan Membantu Konsumen dalam
                                    mendapatkan Aki Berkualitas dalam keadaan
                                    darurat.
                                </li>
                                <li>
                                    Meningkatkan profesionalisme karyawan untuk
                                    memberikan kepuasan pelayanan kepada
                                    pelanggan.
                                </li>
                                <li>
                                    Menyediakan produk berkualitas dengan harga
                                    yang kompetitif.
                                </li>
                                <li>
                                    Tangguh dalam penjualan dan terbaik dalam
                                    pelayanan
                                </li>
                            </ul>
                        </div>
                        <br />
                        <div class="col-md-12">
                            <h3 class="mb3 tc tl-ns">Services</h3>
                            <p>
                                Untuk sebuah Kendaraan kususnya Mobil
                                Kelistrikan adalah hal yang sangat penting.
                                Dalam dunia otomotif mobil, motor dan semua yang
                                menggunakan kelistrikan untuk menggerakkan
                                komponen-komponen sehingga dapat berjalan sesuai
                                dengan kinerja mesin. Adanya suplay listrik yang
                                stabil akan menjaga kestabilan dan kemampuan
                                kerja mesin, aki (accu) sebagai penyimpan energi
                                listrik yang merupakan sumber listrik untuk
                                supply ke starter, sistem pengapian, lampu-lampu
                                dan komponen-komponen lain yang membutuhkan
                                tenaga listrik. Aki (accu) macam-macam kapasitas
                                penyimpanan tenaga listriknya itu tergantung
                                pada kapasitas baterai dalam satuan amper jam
                                (AH). Jika pada kotak baterai tertulis 12 volt
                                65 AH, berarti baterai tersebut memiliki
                                tegangan 12 volt dimana jika baterai tersebut
                                digunakan selama 1 jam dengan arus pemakaian 65
                                amper.
                            </p>
                            <p>
                                Kira-kira 2 sampai 2,1 volt. Tegangan listrik
                                yang dihasilkan sama dengan jumlah tegangan
                                listrik tiap-tiap selnya. Jika baterai memiliki
                                6 sel maka tegangan yang akan dihasilkan 12 volt
                                sampai 12,6 volt, umum setiap sel dengan adanya
                                lubang di kotak accu bagian atas untuk mengisi
                                elektrolit aki.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <h3 class="mb3 tc tl-ns">Sejarah</h3>
                            <p>
                                Halim Accu didirikan oleh Bapak Halim pada tahun
                                2017. Berawal dari ilmu pengetahuan dan
                                pengalaman pak Halim di bidang otomotif membuat
                                beliau menekuni wirausaha baterai atau Aki.
                                Pengalaman dan pengetahuan beliau yang mumpuni
                                membuat didirikannya mendirikan toko ini.
                                Sehingga beliau mengutamakan kepada pelayanan
                                yang baik dan berkualitas.
                            </p>
                            <p>
                                Halim Accu yang berdiri pusat di Jl. Imam Bonjol
                                no.69 Kota Blitar ini sekarang sudah 5 tahun
                                berdiri dan melayani setiap pembeli. Setelah
                                melalui proses yang cukup panjang, kini Halim
                                Accu telah memiliki 2 cabang, outlet kedua
                                berada di kota Malang, Jawa Timur. Selain
                                menjadi toko, Halim Accu menjadi penyedia Aki
                                terlengkap di Blitar dan Malang. Halim Accu juga
                                menjalin kerjasama dengan beberapa Instansi yang
                                bergerak di bidang otomotif.
                            </p>
                            <p>
                                Dengan terjalinnya kerjasama, Halim Accu
                                membuktikan bahwa kami tidak hanya penyedia
                                tetapi sebagai mitra untuk bekerjasama.
                                Pelanggan dan mitra kami selalu merasa puas
                                karena pelayanan dan kinerja kami yang
                                profesional. Motto kami yaitu “Delivery and
                                Ready to use” memotivasi kami untuk selalu
                                berkembang, bersaing, dan bekerjasama sehingga
                                membuat kami penyedia Aki terlengkap di Blitar
                                dan Malang.
                            </p>
                            <p>
                                Kami selalu siap untuk membantu dan memberikan
                                pelayanan terbaik bagi pelanggan kami. Aki
                                kendaraan Anda mengalami masalah? Halim Accu
                                solousinya, Instansi Anda butuh kerjasama? Halim
                                Accu Mitranya. Percayakan kepada kami dan akan
                                kami jaga kepercayaan Anda!
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        @include('templates.front-end.footer')
        @include('templates.front-end.js-loader')
    </body>
</html>
