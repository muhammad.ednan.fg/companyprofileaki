<!DOCTYPE html>
<html lang="en">
    <head>
        @include('templates.front-end.meta-loader')
        @include('templates.front-end.css-loader')
    </head>
    <body>
        <!-- Page Preloder -->
        <div id="preloder">
            <div class="loader"></div>
        </div>
        @include('templates.front-end.header-mobile')
        @include('templates.front-end.header')

        <div
            class="relative flex"
            style="
                background-image: url(assets/img/page-banner.jpg);
                height: 220px;
            "
        >
            <div
                class="absolute w-100 h-100 flex"
                style="height: 200px; background-color: rgba(0, 0, 0, 0.3);"
            >
                <div class="container flex">
                    <div
                        class="flex flex-column items-center justify-center w-100"
                    >
                        <h2 class="white">Our Product</h2>
                        <p class="f4 mt3">
                            <a class="text-white" href="">Halim Accu</a>
                            <span>/</span>
                            <a class="text-white" href="">Our Product</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="container pt5">
            <p class="f4 indent lh-copy">
                Halim Accu Malang hadir menawarkan berbagai macam produk
                Aki/Battery lengkap dan berkualitas. memberikan solusi kebutuhan
                Aki/Battery kendaraan anda di daerah Jawa Timur khususnya di
                kota Blitar, Malang dan sekitarnya dengan mengutamakan kepuasan
                pelanggan kami berkomitmen tetap menjaga kualitas dan kuantitas
                pelayanan kami.
            </p>
            <p class="f4 indent pv4 lh-copy">
                Kami menyediakan berbagai merk dan type Aki/Battery untuk Mobil,
                Motor, Bus, Truck, Kapal, Genset, Kendaraan alat berat dan lain
                sebagainya.
            </p>
        </div>

        <div class="container items-center justify-center flex flex-wrap">
            <img
                class="w-50 mb4-ns"
                src="assets/img/products/merk/yuasa.jpg"
                alt=""
            />
            <img class="w-50" src="assets/img/products/merk/varta.png" alt="" />
            <img
                class="w-50 w-25-ns mb4-ns"
                src="assets/img/products/merk/solite.png"
                alt=""
            />
            <img
                class="w-50 w-25-ns"
                src="assets/img/products/merk/amaron.png"
                alt=""
            />
            <img
                class="w-50 w-25-ns"
                src="assets/img/products/merk/globat.jpg"
                alt=""
            />
            <img class="w-50 w-25-ns" src="assets/img/products/merk/gs.jpg" alt="" />
            <img
                class="w-33 mb4-ns"
                src="assets/img/products/merk/incoe.png"
                alt=""
            />
            <img
                class="w-33"
                src="assets/img/products/merk/motobat.jpg"
                alt=""
            />
            <img
                class="w-33"
                src="assets/img/products/merk/powerzone.png"
                alt=""
            />
        </div>

        <!-- Rooms Section Begin -->
        <section class="rooms-section spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <a href="{{ route('guest.product-detail') }}" class="room-item">
                            <img
                                src="assets/img/products/gsAstra.png"
                                alt=""
                                class="productBg"
                            />
                            <div class="ri-text">
                                <h4>
                                    NS40Z & NS40ZL (36B24R/L)
                                </h4>
                                <h3>
                                    Rp. 1.468.000
                                    <span class="diskon">/ 1.668.000</span>
                                </h3>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td class="r-o">Kapasitas :</td>
                                            <td>35 Ah (Ampere Hour)</td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Tegangan :</td>
                                            <td>12 V (Volt)</td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Kategori :</td>
                                            <td>
                                                Aki kering/MF (Maintenance Free)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Untuk Mobil :</td>
                                            <td>
                                                Honda Brio, Honda Jazz, Toyota
                                                Ayla, Daihatsu Agya, Datsun GO+,
                                                dll.
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="room-item">
                            <img
                                src="assets/img/products/gsPremium.png"
                                alt=""
                                class="productBg"
                            />
                            <div class="ri-text">
                                <h4>
                                    NS40Z & NS40ZL (36B24R/L)
                                </h4>
                                <h3>Rp. 1.468.000</h3>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td class="r-o">Kapasitas :</td>
                                            <td>35 Ah (Ampere Hour)</td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Tegangan :</td>
                                            <td>12V (Volt)</td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Kategori :</td>
                                            <td>Aki basah/Hybrid/Premium</td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Untuk Mobil :</td>
                                            <td>
                                                Honda Brio, Honda Jazz, Toyota
                                                Ayla, Daihatsu Agya, Datsun GO+,
                                                dll.
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="room-item">
                            <img
                                src="assets/img/products/gsHybrid.png"
                                alt=""
                                class="productBg"
                            />
                            <div class="ri-text">
                                <h4>
                                    NS40Z & NS40ZL (36B24R/L)
                                </h4>
                                <h3>Rp. 1.468.000</h3>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td class="r-o">Kapasitas :</td>
                                            <td>45 Ah (Ampere Hour)</td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Tegangan :</td>
                                            <td>12V (Volt)</td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Kategori :</td>
                                            <td>Aki basah/Hybrid/Premium</td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Untuk Mobil :</td>
                                            <td>
                                                Swift, Xpander, Avanza, Xenia,
                                                Innova bensin, Gran max, Carry,
                                                dll.
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="room-item">
                            <img
                                src="assets/img/products/gsMf.png"
                                alt=""
                                class="productBg"
                            />
                            <div class="ri-text">
                                <h4>
                                    NS60 & NS60L (46B24R/L)
                                </h4>
                                <h3>Rp. 1.468.000</h3>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td class="r-o">Kapasitas :</td>
                                            <td>45 Ah (Ampere Hour)</td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Tegangan :</td>
                                            <td>12V (volt)</td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Kategori :</td>
                                            <td>
                                                Aki kering/MF (Maintenance Free)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Untuk Mobil :</td>
                                            <td>
                                                Honda Brio, Honda Jazz, Toyota
                                                Ayla, Daihatsu Agya, Datsun GO+,
                                                dll.
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="room-item">
                            <img
                                src="assets/img/products/incoe.png"
                                alt=""
                                class="productBg"
                            />
                            <div class="ri-text">
                                <h4>
                                    NS60 & NS60L (46B24R/L)
                                </h4>
                                <h3>Rp. 1.468.000</h3>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td class="r-o">Kapasitas :</td>
                                            <td>45 Ah (Ampere Hour)</td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Tegangan :</td>
                                            <td>12V (volt)</td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Kategori :</td>
                                            <td>
                                                Aki kering/MF (Maintenance Free)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Untuk Mobil :</td>
                                            <td>
                                                Swift, Xpander, Avanza, Xenia,
                                                Innova bensin, Gran max, Carry,
                                                dll.
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="room-item">
                            <img
                                src="assets/img/products/incoeGold.png"
                                alt=""
                                class="productBg"
                            />
                            <div class="ri-text">
                                <h4>
                                    NS60 & NS60L (46B24R/L)
                                </h4>
                                <h3>Rp. 1.468.000</h3>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td class="r-o">Kapasitas :</td>
                                            <td>45 Ah (Ampere Hour)</td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Tegangan :</td>
                                            <td>12V (Volt)</td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Kategori :</td>
                                            <td>
                                                Aki kering/MF (Maintenance Free)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="r-o">Untuk Mobil :</td>
                                            <td>
                                                Swift, Xpander, Avanza, Xenia,
                                                Innova bensin, Gran max, Carry,
                                                dll.
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="room-pagination">
                            <a href="#">1</a>
                            <a href="#">2</a>
                            <a href="#"
                                >Next <i class="fa fa-long-arrow-right"></i
                            ></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Rooms Section End -->

        @include('templates.front-end.footer')
        @include('templates.front-end.js-loader')
    </body>
</html>
