<!DOCTYPE html>
<html lang="en">
    <head>
        @include('templates.front-end.meta-loader')
        @include('templates.front-end.css-loader')
    </head>
    <body>
        <!-- Page Preloder -->
        <div id="preloder">
            <div class="loader"></div>
        </div>
        @include('templates.front-end.header-mobile')
        @include('templates.front-end.header')

        <div
            class="relative flex"
            style="
                background-image: url(assets/img/page-banner.jpg);
                height: 220px;
            "
        >
            <div
                class="absolute w-100 h-100 flex"
                style="height: 200px; background-color: rgba(0, 0, 0, 0.3);"
            >
                <div class="container flex">
                    <div
                        class="flex flex-column items-center justify-center w-100"
                    >
                        <h2 class="white">Artikel</h2>
                        <p class="f4 mt3">
                            <a class="text-white" href="">Halim Accu</a>
                            <span>/</span>
                            <a class="text-white" href="">Artikel</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <h3 class="mv5 tc">
            <!DOCTYPE html>
            <html lang="en">
                <head>
                    @include('templates.front-end.meta-loader')
                    @include('templates.front-end.css-loader')
                </head>
                <body>
                    <!-- Page Preloder -->
                    <div id="preloder">
                        <div class="loader"></div>
                    </div>
                    @include('templates.front-end.header-mobile')
                    @include('templates.front-end.header')

                    <div
                        class="relative flex"
                        style="
                            background-image: url(assets/img/page-banner.jpg);
                            height: 200px;
                        "
                    >
                        <div
                            class="absolute w-100 h-100 flex"
                            style="
                                height: 200px;
                                background-color: rgba(0, 0, 0, 0.3);
                            "
                        >
                            <div class="container flex">
                                <div
                                    class="flex flex-column items-center justify-center w-100"
                                >
                                    <h2 class="white">Artikel</h2>
                                    <p class="f4 mt3">
                                        <a class="text-white" href=""
                                            >Halim Accu</a
                                        >
                                        <span>/</span>
                                        <a class="text-white" href=""
                                            >Artikel</a
                                        >
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h3 class="mv5 tc">Mengenal Kode Seri yang ada pada Aki</h3>
                    <section class="">
                        <div class="container">
                            <!-- Services grid -->
                            <div class="mv4 f4 mb5">
                                <div class="indent" style="line-height: 2;">
                                    Pada artikel kali ini Halim Accu coba
                                    memberikan panduan agar Anda dapat membaca
                                    kapasitas dan lebih mengerti aki yang Anda
                                    gunakan secara langsung. Kode aki dituliskan
                                    mengikuti 2 standar/metode:
                                </div>
                                <h3>Japan Industrial Standard (JIS)</h3>
                                <div>
                                    • Contoh: Aki NS40ZLS
                                    <ul>
                                        <li>N = Normal</li>
                                        <li>
                                            S = pengurangan daya aki sebesar 20%
                                        </li>
                                        <li>40 = daya utama aki</li>
                                        <li>
                                            Z = penambahan daya aki sebesar 10%
                                            setelah dikurangi 20% (huruf S
                                            pertama)
                                        </li>
                                        <li>
                                            L = left, artinya pole (kepala aki /
                                            kutub negatif) [-]) berada di
                                            sebelah kiri. Tanpa kode ini pole
                                            pasti berada di sebelah kanan.
                                        </li>
                                        <li>
                                            S = aki memiliki kutub ukuran besar
                                        </li>
                                    </ul>
                                    <p>
                                        Jadi aki NS40ZLS mempunyai daya: 40Ah –
                                        20% + 10% = 32 Ah dengan pole sebelah
                                        kiri dan kepala aki besar.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </section>

                    @include('templates.front-end.footer')
                    @include('templates.front-end.js-loader')
                </body>
            </html>
        </h3>
        <section class="">
            <div class="container">
                <!-- Services grid -->
                <div class="mv4 f4 mb5">
                    <div class="indent" style="line-height: 2;">
                        Akumulator (accu, aki) adalah sebuah alat yang dapat
                        menyimpan energi (umumnya energi listrik) dalam bentuk
                        energi kimia. Contoh-contoh akumulator adalah baterai
                        dan kapasitor. Pada umumnya di Indonesia, kata
                        akumulator (sebagai aki atau accu) hanya dimengerti
                        sebagai “baterai” mobil. Sedangkan di bahasa Inggris,
                        kata akumulator dapat mengacu kepada baterai, kapasitor,
                        kompulsator, dll. di dalam standar internasional setiap
                        satu cell akumulator memiliki tegangan sebesar 2 volt.
                        sehingga aki 12 volt, memiliki 6 cell sedangkan aki 24
                        volt memiliki 12 cell. Aki merupakan sel yang banyak
                        kita jumpai karena banyak digunakan pada sepeda motor
                        maupun mobil. Aki temasuk sel sekunder, karena selain
                        menghasilkan arus listrik, aki juga dapat diisi arus
                        listrik kembali. secara sederhana aki merupakan sel yang
                        terdiri dari elektrode Pb sebagai anode dan PbO2 sebagai
                        katode dengan elektrolit H2SO4.
                    </div>
                </div>
            </div>
        </section>

        @include('templates.front-end.footer')
        @include('templates.front-end.js-loader')
    </body>
</html>
