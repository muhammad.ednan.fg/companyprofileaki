<!-- Header Section Begin -->
<header class="header-section">
        <div class="menu-item">
            <div class="container">
                <div class="row">
                    <div class="col-5 col-lg-2">
                        <div class="logo">
                            <a href="/">
                                <img src="assets/img/company/logo.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-10 d-none d-sm-block">
                        <div class="nav-menu">
                            <nav class="mainmenu">
                                <ul>
                                    <li class="{{ Request::is('home') ? 'active' : '' }}"><a href="{{ route('guest.home') }}">Home</a></li>
                                    <li class="{{ Request::is('products') ? 'active' : '' }}"><a href="{{ route('guest.products') }}">Products</a></li>
                                    <li class="{{ Request::is('outlets') ? 'active' : '' }}"><a href="{{ route('guest.outlets') }}">Outlets</a></li>
                                    <li class="{{ Request::is('about') ? 'active' : '' }}"><a href="{{ route('guest.about') }}">About</a></li>
                                    <li class="{{ Request::is('services') ? 'active' : '' }}"><a href="{{ route('guest.services') }}">Services</a></li>
                                    <li><a href="./blog.html">Artikel</a></li>
                                </ul>
                            </nav>
                            <div class="nav-right">
                                <!-- <span class="charts" style="margin-right: 10px;">
                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                </span> -->
                                <span class="search-switch" style="margin-right: 10px;">
                                    <input type="text" placeholder="Cari Disini..">
                                    <i class="icon_search"></i>
                                </span>   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Header End -->