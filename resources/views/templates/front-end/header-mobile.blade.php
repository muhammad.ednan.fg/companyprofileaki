<!-- Offcanvas Menu Section Begin -->
<div class="offcanvas-menu-overlay"></div>
<div class="canvas-open flex items-center f4">
    <i class="fa fa-shopping-cart white"></i>
    <i class="icon_search white mh3"></i>
    <span class="f2 ml3">
        <i class="icon_menu"></i>
    </span>
</div>
<div class="offcanvas-menu-wrapper">
    <div class="canvas-close">
        <i class="icon_close"></i>
    </div>
    <nav class="mainmenu mobile-menu">
        <ul>
            <li class="{{ Request::is('home') ? 'active' : '' }}">
                <a href="{{ route('guest.home') }}">Home</a>
            </li>
            <li class="{{ Request::is('products') ? 'active' : '' }}">
                <a href="{{ route('guest.products') }}">Products</a>
            </li>
            <li class="{{ Request::is('outlets') ? 'active' : '' }}">
                <a href="{{ route('guest.outlets') }}">Outlets</a>
            </li>
            <li class="{{ Request::is('abouts') ? 'active' : '' }}">
                <a href="{{ route('guest.about') }}">About</a>
            </li>
            <li class="{{ Request::is('services') ? 'active' : '' }}">
                <a href="{{ route('guest.services') }}">Services</a>
            </li>
            <li><a href="./blog.html">Artikel</a></li>
        </ul>
    </nav>
    <div id="mobile-menu-wrap"></div>
    <div class="top-social">
        <a href="#"><i class="fa fa-facebook"></i></a>
        <a href="#"><i class="fa fa-twitter"></i></a>
        <a href="#"><i class="fa fa-tripadvisor"></i></a>
        <a href="#"><i class="fa fa-instagram"></i></a>
    </div>
    <h6>Contact Us</h6>
    <ul class="top-widget">
        <li style="font-size: 14px;">Halim 085-746-546-073 (Blitar)</li>
        <li style="font-size: 14px;">Rifqi 085-731-545-946 (Malang)</li>
    </ul>
</div>
<!-- Offcanvas Menu Section End -->
