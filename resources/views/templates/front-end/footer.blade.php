<!-- WA icon -->
<div class="chat-us w-auto">
    <div class="relative tr">
        <!-- chat us text -->
        <div id="chat-us-head" class="br2 ph2">
            <div>Hubungi Kami</div>
        </div>
        <!-- chat us body -->
        <div
            id="chat-us-body"
            class="bg-white br2"
        >
            <div
                class="white pa3 br2 f5 tracked relative"
                style="background-color: #118fa2;"
            >
                Hubungi kami untuk pemesanan atau pertanyaan.
                <span class="absolute f4" id="chat-us-close">X</span>
            </div>
            <div class="br2 f5">
                <a
                    href="https://wa.me/6285746546073/?text=Saya%20ingin%20membeli%20accu"
                    target="_blank"
                    class="flex items-center mb2 item-wa wa-cabang-blitar pa2 pb0"
                >
                    <img
                        class="mr2"
                        src="assets/img/icon/whatsapp.svg"
                        style="width: 30px;"
                        alt=""
                    />
                    <div>
                        <div>WA Cabang Blitar</div>
                    </div>
                </a>
                <a
                    href="https://wa.me/6285731545946?text=Saya%20ingin%20membeli%20accu"
                    target="_blank"
                    class="flex items-center f6 item-wa wa-cabang-malang pa2"
                >
                    <img
                        class="mr2"
                        src="assets/img/icon/whatsapp.svg"
                        style="width: 30px;"
                        alt=""
                    />
                    <div>
                        <div>WA Cabang Malang</div>
                    </div>
                </a>
            </div>
        </div>
        <img
            src="assets/img/icon/whatsapp.svg"
            id="chat-us-icon"
            class="icon-wa mt2"
            alt=""
        />
    </div>
</div>
<!-- Footer Section Begin -->
<footer class="footer-section">
    <div class="container">
        <div class="footer-text">
            <div class="row">
                <div class="col-lg-4">
                    <div class="ft-about">
                        <div class="logo w-80 center">
                            <a href="/">
                                <img src="assets/img/company/logo.png" alt="" />
                            </a>
                        </div>
                        <!-- <p>We inspire and reach millions of travelers<br /> across 90 local websites</p> -->
                        <div
                            class="flex flex-column"
                            style="line-height: 32px;"
                        >
                            <div>
                                <a
                                class="white link"
                                href="http://instagram.com/akimobilmalang"
                                ><i class="fa fa-instagram mr2"></i>akimobilmalang</a
                            >
                            </div>
                            <div>
                                <a
                                class="white link"
                                href="http://instagram.com/akimobilblitar"
                                ><i class="fa fa-instagram mr2"></i>akimobilblitar</a
                            >
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 offset-lg-1">
                    <div class="ft-contact">
                        <h6>Contact Us</h6>
                        <ul>
                            <li>Halim 085-746-546-073 (Blitar)</li>
                            <li>Rifqi 085-731-545-946 (Malang)</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 offset-lg-1">
                    <div class="ft-newslatter">
                        <h6>Outlets</h6>
                        <p class="mb-0">Halim Accu</p>
                        <p>
                            Jl. Imam Bonjol No.69, Sananwetan, Kec. Sananwetan,
                            Kota Blitar, Jawa Timur 66137
                        </p>
                        <!-- <div class="row">
                            <div class="col-lg-4">
                                <div class="img-instagram">
                                    <img src="https://www.instagram.com/p/B_6c4fKpbiK/media/?size=t" alt="">
                                    <div class="link">
                                        <a href="https://www.instagram.com/p/B_6c4fKpbiK" target="_blank">
                                            <i class="fa fa-instagram mr2"></i>
                                        </a>
                                    </div>
                                </div>                            
                            </div>
                            <div class="col-lg-4">
                                <div class="img-instagram">
                                    <img src="https://www.instagram.com/p/B_6Zu34pu7C/media/?size=t" alt="">
                                    <div class="link">
                                        <a href="https://www.instagram.com/p/B_6Zu34pu7C" target="_blank">
                                            <i class="fa fa-instagram mr2"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="img-instagram">
                                    <img src="https://www.instagram.com/p/CAHBOfIpoc-/media/?size=t" alt="">
                                    <div class="link">
                                        <a href="https://www.instagram.com/p/CAHBOfIpoc-" target="_blank">
                                            <i class="fa fa-instagram mr2"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="img-instagram">
                                    <img src="https://www.instagram.com/p/B95GYMuJFry/media/?size=t" alt="">
                                    <div class="link">
                                        <a href="https://www.instagram.com/p/B95GYMuJFry" target="_blank">
                                            <i class="fa fa-instagram mr2"></i>
                                        </a>
                                    </div>
                                </div>                                
                            </div>
                            <div class="col-lg-4">
                                <div class="img-instagram">
                                    <img src="https://www.instagram.com/p/B_6d2hsBEdX/media/?size=t" alt="">
                                    <div class="link">
                                        <a href="https://www.instagram.com/p/B_6d2hsBEdX" target="_blank">
                                            <i class="fa fa-instagram mr2"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="img-instagram">
                                    <img src="https://www.instagram.com/p/B_6eDYYBo5V/media/?size=t" alt="">
                                    <div class="link">
                                        <a href="https://www.instagram.com/p/B_6eDYYBo5V" target="_blank">
                                            <i class="fa fa-instagram mr2"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div> -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Products</a></li>
                        <li><a href="#">Outlets</a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Services</a></li>
                        <li><a href="#">Artikel</a></li>
                    </ul>
                </div>
                <div class="col-lg-5">
                    <div class="co-text">
                        <p class="invisible">
                            Copyright &copy;
                            <script>
                                document.write(new Date().getFullYear());
                            </script>
                            All rights reserved | This template is made with
                            <i class="fa fa-heart" aria-hidden="true"></i> by
                            <a href="https://colorlib.com" target="_blank"
                                >Colorlib</a
                            >
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer Section End -->
