<!-- Js Plugins -->
    <script src="{!! asset('assets/js/jquery-3.3.1.min.js') !!}"></script>
    <script src="{!! asset('assets/js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('assets/js/jquery.nice-select.min.js') !!}"></script>
    <script src="{!! asset('assets/js/jquery-ui.min.js') !!}"></script>
    <script src="{!! asset('assets/js/jquery.slicknav.js') !!}"></script>
    <script src="{!! asset('assets/js/owl.carousel.min.js') !!}"></script>
    <script src="{!! asset('assets/js/countUp.js') !!}"></script>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script src="https://vjs.zencdn.net/7.8.4/video.js"></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script src="{!! asset('assets/js/main.js') !!}"></script>