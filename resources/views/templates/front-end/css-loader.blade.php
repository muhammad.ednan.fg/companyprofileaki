<!-- Google Font -->
<link
    href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap"
    rel="stylesheet"
/>
<link href="{!! asset('assets/fonts/Cabin.css') !!}" rel="stylesheet" />
<!-- Css Styles -->
<link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/tachyons/4.11.1/tachyons.min.css"
    type="text/css"
/>
<link
    rel="stylesheet"
    href="{!! asset('assets/css/bootstrap.min.css') !!}"
    type="text/css"
/>
<link
    rel="stylesheet"
    href="{!! asset('assets/css/font-awesome.min.css') !!}"
    type="text/css"
/>
<link
    rel="stylesheet"
    href="{!! asset('assets/css/elegant-icons.css') !!}"
    type="text/css"
/>
<link
    rel="stylesheet"
    href="{!! asset('assets/css/flaticon.css') !!}"
    type="text/css"
/>
<link
    rel="stylesheet"
    href="{!! asset('assets/css/owl.carousel.min.css') !!}"
    type="text/css"
/>
<link
    rel="stylesheet"
    href="{!! asset('assets/css/nice-select.css') !!}"
    type="text/css"
/>
<link
    rel="stylesheet"
    href="{!! asset('assets/css/jquery-ui.min.css') !!}"
    type="text/css"
/>
<link
    rel="stylesheet"
    href="{!! asset('assets/css/magnific-popup.css') !!}"
    type="text/css"
/>
<link
    rel="stylesheet"
    href="{!! asset('assets/css/slicknav.min.css') !!}"
    type="text/css"
/>
<link
    rel="stylesheet"
    href="{!! asset('assets/css/style.css') !!}"
    type="text/css"
/>
<link
    rel="stylesheet"
    href="{!! asset('assets/css/aki.css') !!}"
    type="text/css"
/>
<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
<link
    href="https://vjs.zencdn.net/7.8.4/video-js.css"
    rel="stylesheet preload"
/>
<link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
