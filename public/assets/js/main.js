AOS.init();
("use strict");

(function ($) {
    /*------------------
        Preloader
    --------------------*/
    $(window).on("load", function () {
        $(".loader").fadeOut();
        $("#preloder").delay(200).fadeOut("slow");
    });

    /*------------------
        Background Set
    --------------------*/
    $(".set-bg").each(function () {
        var bg = $(this).data("setbg");
        $(this).css("background-image", "url(" + bg + ")");
    });

    //Offcanvas Menu
    $(".canvas-open").on("click", function () {
        $(".offcanvas-menu-wrapper").addClass("show-offcanvas-menu-wrapper");
        $(".offcanvas-menu-overlay").addClass("active");
    });

    $(".canvas-close, .offcanvas-menu-overlay").on("click", function () {
        $(".offcanvas-menu-wrapper").removeClass("show-offcanvas-menu-wrapper");
        $(".offcanvas-menu-overlay").removeClass("active");
    });

    // Search model
    $(".search-switch").on("click", function () {
        $(".search-model").fadeIn(400);
    });

    $(".search-close-switch").on("click", function () {
        $(".search-model").fadeOut(400, function () {
            $("#search-input").val("");
        });
    });

    /*------------------
		Navigation
	--------------------*/
    $(".mobile-menu").slicknav({
        prependTo: "#mobile-menu-wrap",
        allowParentLinks: true,
    });

    /*------------------
        Hero Slider
    --------------------*/
    $(".hero-slider").owlCarousel({
        loop: true,
        margin: 0,
        items: 1,
        dots: true,
        animateOut: "fadeOut",
        animateIn: "fadeIn",
        smartSpeed: 1200,
        autoHeight: false,
        autoplay: true,
        mouseDrag: false,
    });

    /*------------------------
     Slider
    ----------------------- */
    var mySwiper = new window.Swiper("#home-product-slider", {
        // Optional parameters
        loop: true,
        slidesPerView: 3,
        spaceBetween: 30,
        // If we need pagination
        pagination: {
            el: ".swiper-pagination",
        },

        // Navigation arrows
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
    });

    $(".testimonial-slider").owlCarousel({
        items: 1,
        dots: false,
        autoplay: true,
        loop: true,
        smartSpeed: 1200,
        nav: true,
        navText: ["<i class='arrow_left'></i>", "<i class='arrow_right'></i>"],
    });

    /*------------------
		Counter
    --------------------*/
    var isCounterPlayed = false;
    document.addEventListener("aos:in", ({ detail }) => {
        setTimeout(() => {
            counterAreaObserverCallback();
        }, 500);
    });

    function counterAreaObserverCallback() {
        if (!isCounterPlayed) {
            const countUp1 = new CountUp("counter-1", 5234);
            if (!countUp1.error) {
                countUp1.start();
            } else {
                console.error(countUp1.error);
            }
            const countUp2 = new CountUp("counter-2", 5234);
            if (!countUp2.error) {
                countUp2.start();
            } else {
                console.error(countUp2.error);
            }
            const countUp3 = new CountUp("counter-3", 5234);
            if (!countUp3.error) {
                countUp3.start();
            } else {
                console.error(countUp3.error);
            }
            const countUp4 = new CountUp("counter-4", 5234);
            if (!countUp4.error) {
                countUp4.start();
            } else {
                console.error(countUp4.error);
            }
            isCounterPlayed = true;
        }
    }

    /*------------------
		Nice Select
	--------------------*/
    $("select").niceSelect();

    // custom sript

    $("#chat-us-icon").on("click", function () {
        $("#chat-us-head").toggle();
        $("#chat-us-body").toggle();
    });

    $("#chat-us-close").on("click", function () {
        $("#chat-us-head").toggle();
        $("#chat-us-body").hide();
    });
    var player = videojs("my-player", {
        aspectRatio: "4:3",
    });
})(jQuery);
