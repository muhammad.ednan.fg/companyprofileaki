<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GuestController extends Controller {
    
    public function showHomePage(){
        return view('index');
    }

    public function showProductsPage(){
        return view('product');
    }

    public function showOutletsPage(){
        return view('outlets');
    }

    public function showAboutPage(){
        return view('about');
    }

    public function showServicesPage(){
        return view('services');
    }

    public function showAccuDefinitionPage(){
        return view('templates.artikel.pengertian-aki');
    }

    public function showAccuSeriesCodePage(){
        return view('templates.artikel.pengertian-aki');
    }

    public function showDetailProduct(){
        return view('product-detail');
    }
}
