<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'guest', 'as' => 'guest.'], function(){

    Route::get('/', ['as' => 'home', 'uses' => 'GuestController@showHomePage']);
    Route::get('/products', ['as' => 'products', 'uses' => 'GuestController@showProductsPage']);
    Route::get('/outlets', ['as' => 'outlets', 'uses' => 'GuestController@showOutletsPage']);
    Route::get('/about', ['as' => 'about', 'uses' => 'GuestController@showAboutPage']);
    Route::get('/services', ['as' => 'services', 'uses' => 'GuestController@showServicesPage']);
    Route::get('/definisi-aki', ['as' => 'accu-definition', 'uses' => 'GuestController@showAccuDefinitionPage']);
    Route::get('/mengenal-kode-seri-pada-aki', ['as' => 'accu-series-code', 'uses' => 'GuestController@showAccuSeriesCodePage']);
    Route::get('/product-detail', ['as' => 'product-detail', 'uses' => 'GuestController@showDetailProduct']);
    
});


